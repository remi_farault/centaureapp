let checkIpv4Format = (ipString="") => {
    if (ipString != null)
    {
        console.log(ipString);
        let nums = ipString.split('.');
        console.log(nums.length);
        if (nums.length != 4) return false;
        for(const num in nums)
        {
            console.log(num);
            if (num < 0 || num > 255) return false;
        }
        return true;
    }
}

class WebSocketHandler
{
    constructor()
    {
        this.webSocket = null;
        this.ipInput = document.getElementById("ipInput");
        this.ipStatusEl = document.getElementById("ipStatus");
        this.portInput = document.getElementById("portInput");
        this.portStatusEl = document.getElementById("portStatus");
        this.connectButton = document.getElementById("connectButton");
        this.wsStatusEl = document.getElementById("wsStatus");
        this.disconnectButton = document.getElementById("disconnectButton");

        this.ipStatus = false;
        this.portStatus = false;
        this.wsStatus = false;

        this.checkIpInput = this.checkIpInput.bind(this);
        this.ipInput.oninput = this.checkIpInput;
        this.checkPortInput = this.checkPortInput.bind(this);
        this.portInput.oninput = this.checkPortInput;
        this.connectWebSocket = this.connectWebSocket.bind(this);
        this.connectButton.onclick = this.connectWebSocket;
        this.disconnectWebSocket = this.disconnectWebSocket.bind(this);
        this.disconnectButton.onclick = this.disconnectWebSocket;

        this.ipInput.value = '';
        this.portInput.value = '';

        //this.updateView();
    }

    checkIpInput(event)
    {
        this.ipStatus = checkIpv4Format(event.target.value);
        this.updateView();
    }

    checkPortInput(event)
    {
        this.portStatus = (event.target.value > 0 && event.target.value <= 65000);
        this.updateView();
    }

    updateView()
    {
        if(this.ipStatus)
        {
            this.ipStatusEl.innerText = "Correct IP address";
            this.ipStatusEl.classList.remove("is-danger");
            this.ipStatusEl.classList.add("is-success");
        }
        else
        {
            this.ipStatusEl.innerText = "Incorrect IP address";
            this.ipStatusEl.classList.remove("is-success");
            this.ipStatusEl.classList.add("is-danger");
        }

        if(this.portStatus)
        {
            this.portStatusEl.innerText = "Correct port value";
            this.portStatusEl.classList.remove("is-danger");
            this.portStatusEl.classList.add("is-success");
        } 
        else
        {
            this.portStatusEl.innerText = "Incorrect port value";
            this.portStatusEl.classList.remove("is-success");
            this.portStatusEl.classList.add("is-danger");
        }

        this.connectButton.disabled = !(this.ipStatus && this.portStatus);

        if (this.wsStatus)
        {
            this.disconnectButton.disabled = false;
            this.wsStatusEl.innerText = `Connected to ${this.webSocket.url}`;
            this.wsStatusEl.classList.remove("has-text-danger");
            this.wsStatusEl.classList.add("has-text-success");
        }
        else
        {
            this.disconnectButton.disabled = true;
            this.wsStatusEl.innerText = "Not connected";
            this.wsStatusEl.classList.remove("has-text-success");
            this.wsStatusEl.classList.add("has-text-danger");
        }
    }

    connectWebSocket(event) {
        event.preventDefault();
        const url = `ws://${this.ipInput.value}:${this.portInput.value}`;
        this.webSocket = new WebSocket(url);
        
        this.webSocket.onopen = this.socketConnected.bind(this);
        this.webSocket.onclose = this.socketError.bind(this);
        this.webSocket.onerror = this.socketError.bind(this);

        this.updateView();
    }

    socketConnected() {
        this.wsStatus = true;
        this.updateView();
    }

    socketError() {
        this.wsStatus = false;
        this.webSocket = null;
        this.updateView();
    }

    disconnectWebSocket(event)
    {
        event.preventDefault();
        if (this.wsStatus)
        {
            this.webSocket.close();
            this.wsStatus = false;
            this.webSocket = null;
        }
        this.updateView();
    }

    sendData(data) {
        if (this.webSocket != null) {
            this.webSocket.send(data);
            return true;
        } else {
            return false;
        }
    }
}

class App 
{
    constructor()
    {
        this.joystick = new JoyStick("joyDiv", {
            width: 200,
            height: 200,
            internalFillColor: "#3273DC",
            internalLineWidth: 3,
            internalStrokeColor: "#000000",  
            externalLineWidth: 2,
            externalStrokeColor: "#3273DC",
            autoReturnToCenter: true
        });

        this.socketHandler = new WebSocketHandler();

        setInterval(this.sendStickData.bind(this), 1000);
    }

    sendStickData() {
        let x = this.joystick.GetX();
        let y = this.joystick.GetY();
        let data = `(${x},${y})`;
        this.socketHandler.sendData(data);
    }
}


//main

const app = new App();